clear all;
close all;
clc

% Based on:
% https://habr.com/ru/post/166693/

% Honest model with estimation of all availabled model's parameters
% Part 1

pSIZE         = 256;  % Number of samples
pACCELERATION = 0.05; % Input signal acceleration
pSIGMA_E      = 5;    % Model error dispertion
pSIGMA_MU     = 50;   % Sensor error dispersion

x       = zeros(1, pSIZE); % Dirty values
x_clear = zeros(1, pSIZE); % Clear values
z       = zeros(1, pSIZE); % Sensor vector

for i = 1:pSIZE-1
    err_k = normrnd(0, pSIGMA_E); % Model error
    u_k = normrnd(0, pSIGMA_MU);  % Sensor error
    % Change signal
    x_clear(1, i+1) = x_clear(1, i) + pACCELERATION*i;
    x(1, i+1) = x_clear(1, i+1) + err_k;
    % Calculate sensor data
    z(1, i+1) = x(1, i+1) + u_k;
end

% Kalman filtration
E_mean = zeros(1, pSIZE); % Kalman mean errors vector
K_coef = zeros(1, pSIZE); % Kalman coefficients vector
x_opt  = zeros(1, pSIZE); % Results vector

% Base initial. Small influence to result
E_mean(1, 1) = sqrt(pSIGMA_E^2);
x_opt(1, 1)  = z(1, 1);

for i = 1:pSIZE-1
    E_mean(1, i+1) = sqrt( pSIGMA_MU^2 * (E_mean(1, i)^2 + pSIGMA_E^2) / ...
                          (E_mean(1, i)^2 + pSIGMA_MU^2 + pSIGMA_E^2) );
                      
    K_coef(1, i+1) = (E_mean(1, i+1)^2 / pSIGMA_MU^2);

    x_opt(1, i+1)  = (x_opt(1,i)+pACCELERATION*i) * (1 - K_coef(1,i+1)) + K_coef(1,i+1) * z(1, i+1);
end

% Error RMS calculate
rms_before = sqrt(mean((x(:)-z(:)).^2));
rms_after  = sqrt(mean((x(:)-x_opt(:)).^2));

disp('RMS error before Kalman:');
display(round(rms_before));
disp('RMS error after Kalman:');
display(round(rms_after));

% Let's plot results!
figure(1),

subplot(3, 1, 1),
hold on;
plot(x_clear)
plot(x),
plot(z),
legend('Pure signal', 'Signal + model error', 'Signal + model error + sensor error');

subplot(3, 1, 2),
hold on;
plot(E_mean),
plot(K_coef),
legend('Mean error', 'Kalman coefficients');

subplot(3, 1, 3),
hold on;
plot(z),
plot(x_opt),
legend('Signal + model error + sensor error', 'Signal after filtration');


% Unhonest model without estimation of model parameters
% Part 2

pK_STAB = 0.09512;

x_opt_hand  = zeros(1, pSIZE); % Results vector

x_opt_hand(1, 1) = z(1, 1);
for i = 1:pSIZE-1
    x_opt_hand(1, i+1) = x_opt(1,i) * (1 - pK_STAB) + pK_STAB * z(1, i+1);
end

% Error RMS calculate
rms_before = sqrt(mean((x(:)-z(:)).^2));
rms_after  = sqrt(mean((x(:)-x_opt_hand(:)).^2));
rms_versus = sqrt(mean((x_opt(:)-x_opt_hand(:)).^2));

disp('RMS error before Kalman:');
display(round(rms_before));
disp('RMS error after Kalman:');
display(round(rms_after));
disp('Two variants RMS error:');
disp(round(rms_versus));

% Let's plot results!
figure(2),
hold on;
plot(x_clear),
plot(x_opt),
plot(x_opt_hand),
legend('Signal + model error + sensor error', 'Signal after filtration', 'Signal after filtration #2');


